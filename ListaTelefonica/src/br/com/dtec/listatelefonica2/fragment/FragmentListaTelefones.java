package br.com.dtec.listatelefonica2.fragment;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import br.com.dtec.listatelefonica2.R;
import br.com.dtec.listatelefonica2.activity.MostrarTelefones;
import br.com.dtec.listatelefonica2.adapter.ListaTelefonesAdapter;
import br.com.dtec.listatelefonica2.dao.SecaoDAO;
import br.com.dtec.listatelefonica2.entity.Secao;

public class FragmentListaTelefones extends Fragment {
	// private int adapterLayout = android.R.layout.simple_list_item_1;
		private ListaTelefonesAdapter adapter;
		private Context context;
		private Secao secao;

		// private ArrayAdapter<Secao> adapter;
		private SecaoDAO secaoDAO;
		private ListView listaTelefones;

		// private ListaTelefonesAdapter adapter;
		private List<Secao> listaSecoes;
		
		
		public static FragmentListaTelefones newInstance() {
			FragmentListaTelefones fragment = new FragmentListaTelefones();
	        return fragment;
	    }
	    public FragmentListaTelefones() {
	        
	    }
		
		@Override
		public void onCreate(Bundle savedInstanceState) {
		    super.onCreate(savedInstanceState);
		    setHasOptionsMenu(true);
		    
		    
		}
		
		
		
		
		
		@Override
		public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		    super.onCreateOptionsMenu(menu, inflater);
		    inflater.inflate(R.menu.lista_telefones, menu);
		    
		    
		    
		    MenuItem searchItem = menu.findItem(R.id.action_search);
			SearchView searchView = (SearchView) MenuItemCompat
					.getActionView(searchItem);
			searchView.setQueryHint("Pesquisar");
			searchView.setFocusable(true);// Coloca piscando seachview
			searchView.setIconified(false);// Expande a searchview

			searchView.setOnQueryTextListener(new OnQueryTextListener() {

				public boolean onQueryTextChange(String newText) {

					adapter.getFilter().filter(newText);

					return true;
					// Toast.makeText(ActivityListaVeiculos.this, newText,
					// Toast.LENGTH_SHORT).show();

				}

				public boolean onQueryTextSubmit(String query) {
					adapter.getFilter().filter(query);
					// Toast.makeText(ActivityListaVeiculos.this, "Procurando por "
					// + query, Toast.LENGTH_LONG).show();
					return true;
				}
			});

			
		    
		    
		    
		}
		
		
		
		

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			
			super.onCreate(savedInstanceState);
			
			View view = inflater.inflate(R.layout.fragment_lista_telefones, container,
					false);

			//getSupportActionBar().setDisplayHomeAsUpEnabled(true);

			findViewsById(view);
			
			/*TextView emptyView = (TextView) view.findViewById(android.R.id.empty);
			listaTelefones.setEmptyView(emptyView);*/
			
			/*if(listaTelefones == null){
			    Toast.makeText(context, "The list is empty", Toast.LENGTH_SHORT).show();
		}*/
			

			listaTelefones.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					
				    //Codigo abaixo funcionando
					Secao secao = adapter.getItem(position);
					Intent intente_mostrar_telefones = new Intent(getActivity(),MostrarTelefones.class);
					intente_mostrar_telefones.putExtra("chave1",secao.getStr_sigla_secao());
					intente_mostrar_telefones.putExtra("chave2",secao.getStr_nome_secao());
					intente_mostrar_telefones.putExtra("chave3",secao.getStr_sigla_organizacao());
					intente_mostrar_telefones.putExtra("chave4",secao.getStr_nome_organizacao());
					intente_mostrar_telefones.putExtra("chave5",secao.getTelefone1());
					intente_mostrar_telefones.putExtra("chave6",secao.getTelefone2());
					startActivity(intente_mostrar_telefones);
				
					}
		});
			
	return view;
		}
		
		private void findViewsById(View view) {
			listaTelefones = (ListView) view.findViewById(R.id.listatelefones);
			
			
		}
		
		

		

		private void carregarSecoes() {
			//Secao secao = new Secao();
			SecaoDAO secaoDAO = new SecaoDAO(getActivity());
			this.listaSecoes = secaoDAO.recuperarTodos();
			secaoDAO.fecharConexao();
			// this.adapter = new ArrayAdapter<Secao>(this, adapterLayout,
			// listaSecoes);
			this.adapter = new ListaTelefonesAdapter(getActivity(),
					listaSecoes);

			// veiculoDAO = VeiculoDAO.getInstance(getBaseContext());
			// this.listaVeiculos = veiculoDAO.recuperarTodos();

			this.listaTelefones.setAdapter(adapter);
			// listaVeiculos.clear();
			
			
		}
		
		@Override
		public void onResume(){
			super.onResume();
			this.carregarSecoes();
			
			
		}

}
