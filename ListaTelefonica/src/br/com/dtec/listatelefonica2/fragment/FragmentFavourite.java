package br.com.dtec.listatelefonica2.fragment;

import java.util.List;

import br.com.dtec.listatelefonica2.R;
import br.com.dtec.listatelefonica2.activity.MostrarTelefones;
import br.com.dtec.listatelefonica2.adapter.ListaFavoritosTelefonesAdapter;
import br.com.dtec.listatelefonica2.entity.Secao;
import br.com.dtec.listatelefonica2.utils.SharedPreference;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

public class FragmentFavourite extends Fragment {
	
	 private ListView favouriteListView;
	    public static ListaFavoritosTelefonesAdapter favouritsListAdapter;
	    private List<Secao> listaSecoes;
	    SharedPreference sharedPreference;
	    Activity activity;
	    
	    
	    public static FragmentFavourite newInstance() {
	        FragmentFavourite fragment = new FragmentFavourite();
	        return fragment;
	    }
	    public FragmentFavourite() {
	        
	    }
	   /* @Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			activity = getActivity();
		}*/

	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        sharedPreference = new SharedPreference();
	        try {
	        	listaSecoes = sharedPreference.loadFavorites(getActivity());
	        } catch (NullPointerException e){
	            e.printStackTrace();
	        }
	    }

	    @Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
	        View rootView= inflater.inflate(R.layout.fragment_favourite, container, false);
	        favouriteListView =(ListView)rootView.findViewById(R.id.listatelefonesfavoritos);
	        
	       favouriteListView.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					
				    //Codigo abaixo funcionando
					Secao secao = favouritsListAdapter.getItem(position);
					Intent intente_mostrar_telefones = new Intent(getActivity(),MostrarTelefones.class);
					intente_mostrar_telefones.putExtra("chave1",secao.getStr_sigla_secao());
					intente_mostrar_telefones.putExtra("chave2",secao.getStr_nome_secao());
					intente_mostrar_telefones.putExtra("chave3",secao.getStr_sigla_organizacao());
					intente_mostrar_telefones.putExtra("chave4",secao.getStr_nome_organizacao());
					intente_mostrar_telefones.putExtra("chave5",secao.getTelefone1());
					intente_mostrar_telefones.putExtra("chave6",secao.getTelefone2());
					startActivity(intente_mostrar_telefones);
				
					}
		});
	       
	       
	       
	        
	        
	        
	    // Get favorite items from SharedPreferences.
	     			//sharedPreference = new SharedPreference();
	     			//listaSecoes = sharedPreference.loadFavorites(activity);

	     			if (listaSecoes == null) {
	     				/*showAlert(getResources().getString(R.string.no_favorites_items),
	     						getResources().getString(R.string.no_favorites_msg));*/
	     				
	     				Toast.makeText(getActivity(),"N�o h� telefones nos favoritos.", Toast.LENGTH_LONG).show();
	     				
	     				
	     						
	     			} else {

	     				if (listaSecoes.size() == 0) {
	     					/*showAlert(
	     							getResources().getString(R.string.no_favorites_items),
	     							getResources().getString(R.string.no_favorites_msg));*/
	     					Toast.makeText(getActivity(),"N�o h� telefones nos favoritos.", Toast.LENGTH_LONG).show();
	     					
	     				}}
	     
	    
	  
	        return  rootView;
		}
	    
	    
	    /*@Override
		public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		    super.onCreateOptionsMenu(menu, inflater);
		    inflater.inflate(R.menu.favourite_list, menu);
	    }*/
		

	    @Override
	    public void onResume() {
	        super.onResume();
	        Log.e("onResume", "onResume Called");
	        if(listaSecoes != null ) {
	            try {
	                favouritsListAdapter = new ListaFavoritosTelefonesAdapter(getActivity(), listaSecoes);
	                favouriteListView.setAdapter(favouritsListAdapter);
	            } catch (NullPointerException e) {
	                e.printStackTrace();
	            }
	            favouritsListAdapter.notifyDataSetChanged();
	        }
	    }

}
