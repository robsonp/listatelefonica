package br.com.dtec.listatelefonica2.dao;

import java.util.ArrayList;
import java.util.List;
import br.com.dtec.listatelefonica2.entity.Secao;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class SecaoDAO {
	Context context;
	
	 public static final String NOME_TABELA = "secao";
	    public static final String COLUNA_ID = "_id";
	    public static final String COLUNA_SIGLA_SECAO = "str_sigla_secao";
	    public static final String COLUNA_NOME_SECAO = "str_nome_secao";
	    public static final String COLUNA_TELEFONE1 = "telefone1";
	    public static final String COLUNA_TELEFONE2 = "telefone2";
	    public static final String COLUNA_NOME_ORGANIZACAO = "str_nome_organizacao";
	    public static final String COLUNA_SIGLA_ORGANIZACAO = "str_sigla_organizacao";
	 
	 
	     public final static String SCRIPT_CRIACAO_TABELA_SECOES = "CREATE TABLE " + NOME_TABELA + "("
       + COLUNA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," 
		+ COLUNA_SIGLA_SECAO + " TEXT," 
		+ COLUNA_NOME_SECAO + " TEXT,"
       + COLUNA_TELEFONE1 + " TEXT," 
		+ COLUNA_TELEFONE2 + " TEXT,"
		+ COLUNA_SIGLA_ORGANIZACAO + " TEXT,"
       + COLUNA_NOME_ORGANIZACAO + " TEXT " + ")";
	 
	    public static final String SCRIPT_DELECAO_TABELA =  "DROP TABLE IF EXISTS " + NOME_TABELA;
	 
	 
	    private SQLiteDatabase dataBase = null;
	    
	   
	 
	    public SecaoDAO(Context context) {
	        DataBaseHelper persistenceHelper = new DataBaseHelper(context);
	        
	        dataBase = persistenceHelper.getWritableDatabase();
	    }
	 
	    public void salvar(int _id, String sigla_secao, String nome_secao, String telefone1, String telefone2, String nome_organizacao ,String sigla_organizacao) {
	    	ContentValues values = new ContentValues();
	    	
	        values.put(COLUNA_ID,_id);
	        values.put(COLUNA_SIGLA_SECAO,sigla_secao);
	        values.put(COLUNA_NOME_SECAO,nome_secao);
	        values.put(COLUNA_TELEFONE1, telefone1);
	        values.put(COLUNA_TELEFONE2,telefone2);
	        
	        values.put(COLUNA_SIGLA_ORGANIZACAO, sigla_organizacao);
	        values.put(COLUNA_NOME_ORGANIZACAO,nome_organizacao);
	        
	        
	        dataBase.insert(NOME_TABELA, null, values);
	    }
	    
	   
	   
	    
	    
	    
	    public boolean atualizar(int _id, String sigla_secao, String nome_secao, String telefone1, String telefone2, String nome_organizacao ,String sigla_organizacao) {
	        ContentValues values = new ContentValues();
	        values.put(COLUNA_ID,_id);
	        values.put(COLUNA_SIGLA_SECAO,sigla_secao);
	        values.put(COLUNA_NOME_SECAO,nome_secao);
	        values.put(COLUNA_TELEFONE1, telefone1);
	        values.put(COLUNA_TELEFONE2,telefone2);
	        
	        values.put(COLUNA_SIGLA_ORGANIZACAO, sigla_organizacao);
	        values.put(COLUNA_NOME_ORGANIZACAO,nome_organizacao);
	        return dataBase.update(NOME_TABELA, values, COLUNA_ID + "=" + _id, null) > 0;
	      }
	    
	    
	 
	    
	    
	    public List<Secao> recuperarTodos() {
	        //String queryReturnAll = "SELECT * FROM " + NOME_TABELA + " ORDER BY " + COLUNA_SIGLA_ORGANIZACAO;
	    	String queryReturnAll = "SELECT * FROM " + NOME_TABELA + " ORDER BY " + COLUNA_NOME_ORGANIZACAO;
	        Cursor cursor = dataBase.rawQuery(queryReturnAll, null);
	        List<Secao> secoes = construirSecaoPorCursor(cursor);
	 
	        return secoes;
	    }
	    
	   
	    
	    /*public boolean exists(int id) {
	    	
	    	
	        SQLiteDatabase sdb = this.getReadableDatabase();
	        Cursor cursor = sdb.query(false, "secao", new String[] {"_id"}, "_id=?", new String[] {""+id}, null, null, null, null);
	        
	        boolean exists = false;
	        
	        if ((cursor.getCount()) > 0) {
	            exists = true;
	        }
	        cursor.close();
	        sdb.close();
	        
	        return exists;
	    }
	    */
	   
	  
	    	
	    	
	    	
	  //codigo anterior
	    
	   /* public void deletar(Secao secao) {
	 
	        String[] valoresParaSubstituir = {
	                String.valueOf(secao.get_id())
	        };
	 
	        dataBase.delete(NOME_TABELA, COLUNA_ID + " =  ?", valoresParaSubstituir);
	    }*/
	    
	    public boolean deletar(int COLUNA_ID) {
	   	
	       return dataBase.delete(NOME_TABELA, COLUNA_ID + " =" + COLUNA_ID, null)>0;
	    }
	 
	    public void editar(Secao secao) {
	        ContentValues valores = gerarContentValeuesSecao(secao);
	 
	        String[] valoresParaSubstituir = {
	                String.valueOf(secao.get_id())
	        };
	 
	        dataBase.update(NOME_TABELA, valores, COLUNA_ID + " = ?", valoresParaSubstituir);
	    }
	 
	    public void fecharConexao() {
	        if(dataBase != null && dataBase.isOpen())
	            dataBase.close(); 
	    }
	 
	 
	    private List<Secao> construirSecaoPorCursor(Cursor cursor) {
	        List<Secao> secoes = new ArrayList<Secao>();
	        if(cursor == null)
	            return secoes;
	         
	        try {
	 
	            if (cursor.moveToFirst()) {
	                do {
	 
	                    int indexID = cursor.getColumnIndex(COLUNA_ID);
	                    int indexSiglaSecao = cursor.getColumnIndex(COLUNA_SIGLA_SECAO);
	                    int indexNomeSecao = cursor.getColumnIndex(COLUNA_NOME_SECAO);
	                    int indexSiglaOrganizacao = cursor.getColumnIndex(COLUNA_SIGLA_ORGANIZACAO);
	                    int indexNomeOrganizacao = cursor.getColumnIndex(COLUNA_NOME_ORGANIZACAO);
	                    int indexTelefone1 = cursor.getColumnIndex(COLUNA_TELEFONE1);
	                    int indexTelefone2 = cursor.getColumnIndex(COLUNA_TELEFONE2);
	                    
	                    
	                    int _id = cursor.getInt(indexID);
	                    String str_sigla_secao = cursor.getString(indexSiglaSecao);
	                    String str_nome_secao = cursor.getString(indexNomeSecao);
	                    String str_sigla_organizacao = cursor.getString(indexSiglaOrganizacao);
	                    String str_nome_organizacao = cursor.getString(indexNomeOrganizacao);
	                    String telefone1 = cursor.getString(indexTelefone1);
	                    String telefone2 = cursor.getString(indexTelefone2);
	                    
	                    
	                    //Mostra os itens do objeto secao
	                    Secao secao = new Secao(_id,str_sigla_secao, str_nome_secao,str_sigla_organizacao,str_nome_organizacao, telefone1,telefone2);
	                    secoes.add(secao);
	 
	                } while (cursor.moveToNext());
	            }
	            
	         
	             
	        } finally {
	            cursor.close();
	        }
	        return secoes;
	    }
	 
	    private ContentValues gerarContentValeuesSecao(Secao secao) {
	        ContentValues values = new ContentValues();
	        values.put(COLUNA_ID, secao.get_id());
	        values.put(COLUNA_SIGLA_SECAO, secao.getStr_sigla_secao());
	        values.put(COLUNA_NOME_SECAO, secao.getStr_nome_secao());
	        values.put(COLUNA_SIGLA_ORGANIZACAO, secao.getStr_sigla_organizacao());
	        values.put(COLUNA_NOME_ORGANIZACAO, secao.getStr_nome_organizacao());
	        values.put(COLUNA_TELEFONE1, secao.getTelefone1());
	        values.put(COLUNA_TELEFONE2, secao.getTelefone2());
	        
	        return values;
	    }}