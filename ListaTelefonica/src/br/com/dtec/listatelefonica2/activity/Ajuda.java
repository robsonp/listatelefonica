package br.com.dtec.listatelefonica2.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import br.com.dtec.listatelefonica2.R;
import android.view.View.OnClickListener;

public class Ajuda extends ActionBarActivity {

	Button email;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ajuda);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		email = (Button) findViewById(R.id.id_email);

		email.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				enviarEmail();

			}
		});
	}

	// Enviar email
	protected void enviarEmail() {
		Log.i("EMAIL","Enviando email");

		String[] TO = { "robsonp1000@gmail.com" };
		String[] CC = { "robson.passos@pm.pe.gov.br" };

		Intent emailIntent = new Intent(Intent.ACTION_SEND);
		emailIntent.setData(Uri.parse("mailto:"));
		emailIntent.setType("text/plain");

		emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
		emailIntent.putExtra(Intent.EXTRA_CC, CC);
		emailIntent.putExtra(Intent.EXTRA_SUBJECT,
				"Cr�ticas, sugest�es ou d�vidas ? ");
		emailIntent.setType("message/rfc822");// S� pede cliente de email
		// emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message goes here");

		try {
			startActivity(Intent.createChooser(emailIntent,
					"Escolha um cliente de email..."));
			finish();
			Log.i("EMAIL","Termino envio do email");
		} catch (android.content.ActivityNotFoundException ex) {
			Toast.makeText(Ajuda.this, "N�o h� nenhum cliente de email. ",
					Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ajuda, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
