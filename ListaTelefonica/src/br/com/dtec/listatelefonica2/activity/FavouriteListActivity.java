package br.com.dtec.listatelefonica2.activity;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import br.com.dtec.listatelefonica2.R;
import br.com.dtec.listatelefonica2.fragment.FragmentFavourite;

public class FavouriteListActivity extends ActionBarActivity {
     
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_favourite_list);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		
		
        FragmentManager manager = getFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        FragmentFavourite fragmentFavourite = FragmentFavourite.newInstance();

        if (manager.findFragmentByTag("fragment_fav") == null) {
          
            ft.replace(R.id.main_content, fragmentFavourite, "frament_fav");
            ft.commit();
        }
	}
	
	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		getMenuInflater().inflate(R.menu.favourite_list, menu);
		return true;
	}*/

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
