package br.com.dtec.listatelefonica2.activity;

import java.util.List;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import br.com.dtec.listatelefonica2.R;
import br.com.dtec.listatelefonica2.adapter.ListaTelefonesAdapter;
import br.com.dtec.listatelefonica2.entity.Secao;

public class MostrarTelefones extends ActionBarActivity {

	ListaTelefonesAdapter adapter;
	private List<Secao> listaSecoes;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mostrar_telefones);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		ListaTelefonesAdapter adapter = new ListaTelefonesAdapter(
				getBaseContext(), listaSecoes);

		//Secao secao = new Secao();

		// Codigo Funcionando
		TextView txtsiglasecao = (TextView) findViewById(R.id.idSiglaSecao);
		TextView txtnomesecao = (TextView) findViewById(R.id.idNomeSecao);
		TextView txtsiglaorganizacao = (TextView) findViewById(R.id.idStrSiglaOrganizacao);
		TextView txtnomeorganizacao = (TextView) findViewById(R.id.idStrNomeOrganizacao);
		final TextView txttelefone1 = (TextView) findViewById(R.id.idTelefone1);
		TextView txttelefonemostrar = (TextView) findViewById(R.id.idTelefone_mostrar);
		final TextView txttelefone2 = (TextView) findViewById(R.id.idTelefone2);
		
		txttelefone2.setVisibility(View.GONE);   
			

		Intent i = getIntent();
		if (i != null) {
			txtsiglasecao.setText(i.getStringExtra("chave1"));
			txtnomesecao.setText(i.getStringExtra("chave2"));
			txtsiglaorganizacao.setText(i.getStringExtra("chave3"));
			txtnomeorganizacao.setText(i.getStringExtra("chave4"));
			txttelefone1
					.setText(adapter.formatPhone(i.getStringExtra("chave5")));
			
			
			
			//txttelefone2.setText(adapter.formatPhone(i.getStringExtra("chave6")));
				
		}

		txttelefone1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent_call = new Intent(Intent.ACTION_DIAL);
				intent_call.setData(Uri.parse("tel:"
						+ txttelefone1.getText().toString()));
				startActivity(intent_call);

			}
		});

		/*txttelefone2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent_call = new Intent(Intent.ACTION_DIAL);
				intent_call.setData(Uri.parse("tel:"
						+ txttelefone2.getText().toString()));
				startActivity(intent_call);

			}
		});*/
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.mostrar_telefones, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
