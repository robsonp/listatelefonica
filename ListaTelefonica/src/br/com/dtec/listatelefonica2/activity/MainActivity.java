package br.com.dtec.listatelefonica2.activity;

import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import br.com.dtec.listatelefonica2.R;
import br.com.dtec.listatelefonica2.adapter.ListaTelefonesAdapter;
import br.com.dtec.listatelefonica2.dao.DataBaseHelper;
import br.com.dtec.listatelefonica2.dao.SecaoDAO;
import br.com.dtec.listatelefonica2.entity.Secao;
import br.com.dtec.listatelefonica2.webservice.MyApplication;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

public class MainActivity extends ActionBarActivity {
	private ListaTelefonesAdapter adapter;
	 //static String url="http://10.0.2.2:86/scriptcase/app/ListaTelefonica/json_secao/json_secao.php?nmgp_outra_jan=true&nmgp_start=SC";
	static String url = "http://sistemas.pm.pe.gov.br/listatelefonica/json_secao/json_secao.php";
	static ProgressDialog PD;
	static SecaoDAO secaoDAO;
	static Secao secao;
	static DataBaseHelper myDbHelper;
	public static final String KEY_PREFS_FIRST_LAUNCH = "primeira_inicializacao";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		secaoDAO = new SecaoDAO(MainActivity.this);

		PD = new ProgressDialog(MainActivity.this);
		PD.setMessage("Aguarde");
		PD.setTitle("Atualizando");
		PD.setCancelable(true);

		myDbHelper = new DataBaseHelper(this);
		secao = new Secao();
		// SharedPreferences sharedPref =
		// PreferenceManager.getDefaultSharedPreferences(this);
		SharedPreferences sharedPref = getSharedPreferences(
				getString(R.string.preference_file_key_name),
				Context.MODE_PRIVATE);

		if (sharedPref.getBoolean(KEY_PREFS_FIRST_LAUNCH, true)) {

			try {

				myDbHelper.createDataBase();

			} catch (IOException ioe) {

				throw new Error("Unable to create database");

			}
			SharedPreferences.Editor prefEditor = sharedPref.edit();
			prefEditor.putBoolean(KEY_PREFS_FIRST_LAUNCH, false);
			prefEditor.commit();
		} else {

			try {

				myDbHelper.openDataBase();

			} catch (SQLException sqle) {

				throw sqle;
			}
		}

		Button btpesquisar = (Button) findViewById(R.id.btPesquisar);
		// Button btpreferencias = (Button) findViewById(R.id.btPreferencias);
		// Funcionando
		Button btfavoritos = (Button) findViewById(R.id.btFavoritos);

		btpesquisar.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent_listar = new Intent(MainActivity.this,
						ListaTelefones.class);
				startActivity(intent_listar);

			}
		});

		btfavoritos.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent i = new Intent(MainActivity.this,
						FavouriteListActivity.class);
				startActivity(i);

			}
		});

		// Funcionando
		/*
		 * btpreferencias.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) {
		 * 
		 * Intent intent_preferencias = new Intent(MainActivity.this,
		 * Preferencias.class); startActivity(intent_preferencias);
		 * 
		 * } });
		 */

	}

	public static void MakeJsonArrayReq() {

		// PD.show();

		JsonArrayRequest jreq = new JsonArrayRequest(url,
				new Response.Listener<JSONArray>() {

					@Override
					public void onResponse(JSONArray response) {

						for (int i = 0; i < response.length(); i++) {
							try {
								JSONObject jo = response.getJSONObject(i);

								int _id = jo.getInt("_id");
								String sigla_secao = jo
										.getString("sigla_secao");
								String nome_secao = jo.getString("nome_secao");
								String sigla_organizacao = jo
										.getString("sigla_organizacao");
								String nome_organizacao = jo
										.getString("nome_organizacao");
								String telefone1 = jo.getString("telefone1");
								String telefone2 = jo.getString("telefone2");
								

								if (myDbHelper.exists(_id)) {
									secaoDAO.atualizar(_id, sigla_secao,
											nome_secao, telefone1, telefone2,
											nome_organizacao, sigla_organizacao);
									
									secaoDAO.deletar(_id);
								} else {
								
									secaoDAO.salvar(_id, sigla_secao,
											nome_secao, telefone1, telefone2,
											nome_organizacao, sigla_organizacao);
						
									
								}
								
								

							}

							catch (JSONException e) {
								e.printStackTrace();
							}
						}

						PD.dismiss();

						/*
						 * Toast.makeText(getApplicationContext(),
						 * "Seus dados foram atualizados",
						 * Toast.LENGTH_SHORT).show();
						 */

					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
					}
				});

		MyApplication.getInstance().addToReqQueue(jreq, "jreq");
	}

	public void StartIntentService() {
		// Executando um service...
		Intent i = new Intent(MainActivity.this,
				ServiceAtualizarTelefones.class);
		i.putExtra("pergunta", "IntentService foi executado?");
		// Start the service
		startService(i);
	}

	// Verifica se tem conexao com a internet
	public boolean verificaConexao() {
		boolean conectado;
		ConnectivityManager conectivtyManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (conectivtyManager.getActiveNetworkInfo() != null
				&& conectivtyManager.getActiveNetworkInfo().isAvailable()
				&& conectivtyManager.getActiveNetworkInfo().isConnected()) {
			conectado = true;
		} else {
			conectado = false;
		}
		return conectado;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_settings:
			Intent intent_configuracao = new Intent(this, Configuracao.class);
			startActivity(intent_configuracao);
			return true;

		case R.id.atualizar:

			if (verificaConexao()) {
				// MakeJsonArrayReq();
				PD.show();
				/*
				 * Toast.makeText(getApplicationContext(),
				 * "Seus dados est�o sendo atualizados",
				 * Toast.LENGTH_SHORT).show();
				 */
				Log.d("TAG-IntentService", "Seus dados est�o sendo atualizados");
				StartIntentService();
				Log.d("TAG-IntentService", "Seus dados foram atualizados");
				/*
				 * Toast.makeText(getApplicationContext(),
				 * "Seus dados foram atualizados", Toast.LENGTH_SHORT).show();
				 */

				// Toast.makeText(getApplicationContext(),"Conectado a intenet",
				// Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(getApplicationContext(),
						"Voc� n�o possui conex�o � Internet", Toast.LENGTH_LONG)
						.show();
			}

			/*
			 * if (verificaConexao()) {
			 * 
			 * new Thread() {
			 * 
			 * @Override public void run() { MainActivity.this.runOnUiThread(new
			 * Runnable() {
			 * 
			 * @Override public void run() { MakeJsonArrayReq();
			 * 
			 * } });
			 * 
			 * 
			 * } }.start();
			 * 
			 * 
			 * 
			 * // Toast.makeText(getApplicationContext(),"Conectado a intenet",
			 * // Toast.LENGTH_LONG).show(); } else {
			 * Toast.makeText(getApplicationContext(),
			 * "Voc� n�o possui conex�o � Internet", Toast.LENGTH_LONG) .show();
			 * }
			 */

			/*
			 * Intent intent_search_main = new Intent(this,
			 * ListaTelefones.class); startActivity(intent_search_main);
			 */return true;
		case R.id.search_main:

			Intent intent_search_main = new Intent(this, ListaTelefones.class);
			startActivity(intent_search_main);
			return true;

		case R.id.ajuda:

			Intent intent_ajuda = new Intent(this, Ajuda.class);
			startActivity(intent_ajuda);
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}

	}
}
