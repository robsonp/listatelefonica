package br.com.dtec.listatelefonica2.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.dtec.listatelefonica2.entity.Secao;

import com.google.gson.Gson;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SharedPreference {
	
	public static final String PREFS_NAME = "ListaTelefones";
    public static final String FAVORITES = "Favorite";

    public SharedPreference() {
        super();
    }

    public void storeFavorites(Context context, List<Secao> favorites) {
        SharedPreferences settings;
        Editor editor;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(favorites);

        editor.putString(FAVORITES, jsonFavorites);

        editor.commit();
 }

    public ArrayList<Secao> loadFavorites(Context context) {
        SharedPreferences settings;
        List<Secao> favorites;

        settings = context.getSharedPreferences(PREFS_NAME,Context.MODE_PRIVATE);
        if (settings.contains(FAVORITES)) {
            String jsonFavorites = settings.getString(FAVORITES, null);
            Gson gson = new Gson();
            Secao[] favoriteItems = gson.fromJson(jsonFavorites,Secao[].class);
            favorites = Arrays.asList(favoriteItems);
            favorites = new ArrayList<Secao>(favorites);
        } else
            return null;

        return (ArrayList<Secao>) favorites;
    }


    public void addFavorite(Context context, Secao secao) {
        List<Secao> favorites = loadFavorites(context);
        if (favorites == null)
            favorites = new ArrayList<Secao>();
        favorites.add(secao);
        storeFavorites(context, favorites);
    }

    public void removeFavorite(Context context, Secao secao) {
        ArrayList<Secao> favorites = loadFavorites(context);
        if (favorites != null) {
            favorites.remove(secao);
            storeFavorites(context, favorites);
        }
    }


}
