package br.com.dtec.listatelefonica2.entity;

import java.io.Serializable;

public class Secao implements Serializable {

	private int _id;
	private String str_sigla_secao;
	private String str_nome_secao;
	private String str_sigla_organizacao;
	private String str_nome_organizacao;
	private String telefone1;
	private String telefone2;
	
	
	
	
	public Secao(){
		
	}
	
	
	
	public Secao(int _id, String str_sigla_secao, String str_nome_secao,
			String str_sigla_organizacao, String str_nome_organizacao,
			String telefone1, String telefone2) {
		super();
		this._id = _id;
		this.str_sigla_secao = str_sigla_secao;
		this.str_nome_secao = str_nome_secao;
		this.str_sigla_organizacao = str_sigla_organizacao;
		this.str_nome_organizacao = str_nome_organizacao;
		this.telefone1 = telefone1;
		this.telefone2 = telefone2;
		
	}

	public int get_id() {
		return _id;
	}
	public void set_id(int _id) {
		this._id = _id;
	}
	public String getStr_sigla_secao() {
		return str_sigla_secao;
	}
	public void setStr_sigla_secao(String str_sigla_secao) {
		this.str_sigla_secao = str_sigla_secao;
	}
	public String getStr_nome_secao() {
		return str_nome_secao;
	}
	public void setStr_nome_secao(String str_nome_secao) {
		this.str_nome_secao = str_nome_secao;
	}
	public String getStr_nome_organizacao() {
		return str_nome_organizacao;
	}
	public void setStr_nome_organizacao(String str_nome_organizacao) {
		this.str_nome_organizacao = str_nome_organizacao;
	}
	public String getTelefone1() {
		return telefone1;
	}
	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}
	public String getTelefone2() {
		return telefone2;
	}
	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}
	
	public String getStr_sigla_organizacao() {
		return str_sigla_organizacao;
	}
	public void setStr_sigla_organizacao(String str_sigla_organizacao) {
		this.str_sigla_organizacao = str_sigla_organizacao;
	}

	@Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Secao other = (Secao) obj;
        if (_id != other._id)
            return false;
        return true;
    }

	/*@Override
	public String toString() {
		return str_organizacao + " - " + str_sigla + " - " + str_nome + " - " + telefone1;
		
	}*/

}
