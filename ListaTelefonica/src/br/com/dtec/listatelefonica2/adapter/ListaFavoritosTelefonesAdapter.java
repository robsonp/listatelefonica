package br.com.dtec.listatelefonica2.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import br.com.dtec.listatelefonica2.R;
import br.com.dtec.listatelefonica2.entity.Secao;
import br.com.dtec.listatelefonica2.utils.SharedPreference;

public class ListaFavoritosTelefonesAdapter extends BaseAdapter implements Filterable
		 {

	private List<Secao> listafavoritosSecoes;// listaVeiculos;
	private List<Secao> mOriginalList;
	//LayoutInflater inflater;
	SharedPreference sharedPreference;
	private Context context;

	public ListaFavoritosTelefonesAdapter(Context context,
			List<Secao> listaSecoes) {
		this.listafavoritosSecoes = listaSecoes;// Items que preenchem o listview
		//inflater = LayoutInflater.from(context);// Pega o layout do item
		this.context = context;
		sharedPreference = new SharedPreference();
	}

	@Override
	public int getCount() {// Retorna a quantidade de itens

		return listafavoritosSecoes.size();
	}

	@Override
	public Secao getItem(int posicao) {// Retorna o item de uma posi��o
										// especifica

		return listafavoritosSecoes.get(posicao);
	}

	@Override
	public long getItemId(int posicao) {// Retorna o Id de um item de uma
										// posicao especifica

		return posicao;
	}

	/*
	 * public void clearlistview() { listaSecoes.clear();
	 * this.notifyDataSetChanged(); }
	 */

	// retorna a View com as informa��es posicionadas de acordo com o layout
	// montado no arquivo item_listview.xml
	@Override
	public View getView(final int posicao, View convertView, ViewGroup parent) {

		View view;
		final ViewHolder holder;
		 LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

		if (convertView == null) {
			view = inflater.inflate(R.layout.item_listview, parent, false);
			holder = new ViewHolder();
			holder.str_sigla_secao = (TextView) view
					.findViewById(R.id.idSiglaSecao);
			holder.str_nome_secao = (TextView) view
					.findViewById(R.id.idNomeSecao);
			holder.str_sigla_organizacao = (TextView) view
					.findViewById(R.id.idStrSiglaOrganizacao);
			holder.str_nome_organizacao = (TextView) view
					.findViewById(R.id.idStrNomeOrganizacao);
			holder.telefone1 = (TextView) view.findViewById(R.id.idTelefone1);
			holder.telefone2 = (TextView) view.findViewById(R.id.idTelefone2);
			holder.btnFavourite = (ImageView) view
					.findViewById(R.id.favouritesToggle);

			view.setTag(holder);

		} else {
			view = convertView;
			holder = (ViewHolder) view.getTag();
		}

		Secao secao = listafavoritosSecoes.get(posicao);// Pega o item de acordo com a
												// posi��o.
		holder.str_sigla_secao.setText(secao.getStr_sigla_secao());
		holder.str_nome_secao.setText(secao.getStr_nome_secao());
		holder.str_sigla_organizacao.setText(secao.getStr_sigla_organizacao());
		holder.str_nome_organizacao.setText(secao.getStr_nome_organizacao());
		holder.telefone1.setText(secao.getTelefone1());
		holder.telefone2.setText(secao.getTelefone2());
		holder.btnFavourite.setImageResource(R.drawable.ic_favorite);

		/*if (checkFavoriteItem(secao)) {
			holder.btnFavourite.setImageResource(R.drawable.ic_favorite);
			holder.btnFavourite.setTag("active");
		} else {
			holder.btnFavourite
					.setImageResource(R.drawable.ic_favorite_outline);
			holder.btnFavourite.setTag("deactive");
		}*/

		holder.btnFavourite.setOnClickListener(new View.OnClickListener() {
			
			 @Override
             public void onClick(View view) {
                 sharedPreference.removeFavorite(context, listafavoritosSecoes.get(posicao));
                 //Toast.makeText(context,context.getResources().getString(R.string.remove_favr),Toast.LENGTH_SHORT).show();
                 listafavoritosSecoes.remove(listafavoritosSecoes.get(posicao));
                 holder.btnFavourite.setImageResource(R.drawable.ic_favorite_outline);
                 notifyDataSetChanged();
             }
			
	});

		return view;
	}

	/*public boolean checkFavoriteItem(Secao checkProduct) {
		boolean check = false;
		List<Secao> favorites = sharedPreference.loadFavorites(context);
		if (favorites != null) {
			for (Secao product : favorites) {
				if (product.equals(checkProduct)) {
					check = true;
					break;
				}
			}
		}
		return check;
	}*/

	static class ViewHolder {
		TextView str_sigla_secao;
		TextView str_nome_secao;
		TextView str_sigla_organizacao;
		TextView str_nome_organizacao;
		TextView telefone1;
		TextView telefone2;
		ImageView btnFavourite;

		public ViewHolder() {

		}
	}

	@Override
	public Filter getFilter() {
		Filter filter = new Filter() {

			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {

				listafavoritosSecoes = (List<Secao>) results.values;

				notifyDataSetChanged();

			}

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {

				FilterResults results = new FilterResults();
				List<Secao> FilteredArrList = new ArrayList<Secao>();

				if (mOriginalList == null) {
					mOriginalList = new ArrayList<Secao>(listafavoritosSecoes);
				}

				if (constraint == null || constraint.length() == 0) {
					// c�digo se por acaso o filtro for vazio
					// caso queira voltar a lista original use esse c�digo
					// results.count = mOriginalList.size();
					// results.values = mOriginalList;
					// caso queria deix�-la vazia:
					listafavoritosSecoes.clear();
					results.count = 0;
					results.values = new ArrayList<Secao>();

				} else {
					constraint = constraint.toString().toLowerCase();
					for (int i = 0; i < mOriginalList.size(); i++) {
						Secao v = mOriginalList.get(i);
						String sigla_secao = v.getStr_sigla_secao();
						String nome_secao = v.getStr_nome_secao();
						String sigla_organizacao = v.getStr_sigla_organizacao();
						String nome_organizacao = v.getStr_nome_organizacao();
						String telefone1 = v.getTelefone1();
						String telefone2 = v.getTelefone2();

						if (sigla_secao.toLowerCase().startsWith(
								constraint.toString())) {
							FilteredArrList.add(v);
						}

						if (nome_secao.toLowerCase().startsWith(
								constraint.toString())) {
							FilteredArrList.add(v);
						}

						if (sigla_organizacao.toLowerCase().startsWith(
								constraint.toString())) {
							FilteredArrList.add(v);
						}

						if (nome_organizacao.toLowerCase().startsWith(
								constraint.toString())) {
							FilteredArrList.add(v);
						}

					}

					results.count = FilteredArrList.size();
					results.values = FilteredArrList;
				}
				return results;
			}
		};
		return filter;
	}
}
