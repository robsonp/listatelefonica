package br.com.dtec.listatelefonica2.adapter;

import java.util.ArrayList;
import java.util.List;

import br.com.dtec.listatelefonica2.R;
import br.com.dtec.listatelefonica2.entity.Secao;
import br.com.dtec.listatelefonica2.utils.SharedPreference;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ListaTelefonesAdapter extends BaseAdapter implements Filterable {

	private List<Secao> listaSecoes;// listaVeiculos;
	private List<Secao> mOriginalList;
	LayoutInflater inflater;
	SharedPreference sharedPreference;
	Activity activity;
	private Context context;
	public ListaTelefonesAdapter(Context context, List<Secao> listaSecoes) {
		this.listaSecoes = listaSecoes;// Items que preenchem o listview
		inflater = LayoutInflater.from(context);// Pega o layout do item
		sharedPreference = new SharedPreference();
		this.context = context;
	}

	@Override
	public int getCount() {// Retorna a quantidade de itens

		return listaSecoes.size();
	}

	@Override
	public Secao getItem(int posicao) {// Retorna o item de uma posi��o
										// especifica

		return listaSecoes.get(posicao);
	}

	@Override
	public long getItemId(int posicao) {// Retorna o Id de um item de uma
										// posicao especifica

		return posicao;
	}

	/*
	 * public void clearlistview() { listaSecoes.clear();
	 * this.notifyDataSetChanged(); }
	 */

	// retorna a View com as informa��es posicionadas de acordo com o layout
	// montado no arquivo item_listview.xml
	@Override
	public View getView(final int posicao, View convertView, ViewGroup parent) {

		
		final ViewHolder holder;

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.item_listview, parent, false);
			
			
			
			holder = new ViewHolder();
			
			//holder._id = () convertView.findViewById(R.id.id_Id);
			holder.str_sigla_secao = (TextView) convertView
					.findViewById(R.id.idSiglaSecao);
			holder.str_nome_secao = (TextView) convertView
					.findViewById(R.id.idNomeSecao);
			holder.str_sigla_organizacao = (TextView) convertView
					.findViewById(R.id.idStrSiglaOrganizacao);
			holder.str_nome_organizacao = (TextView) convertView
					.findViewById(R.id.idStrNomeOrganizacao);
			holder.telefone1 = (TextView) convertView.findViewById(R.id.idTelefone1);
			holder.telefone2 = (TextView) convertView.findViewById(R.id.idTelefone2);
			holder.btnFavourite = (ImageView) convertView
                    .findViewById(R.id.favouritesToggle);

			convertView.setTag(holder);

		} else {
		
			holder = (ViewHolder) convertView.getTag();
		}

		//Secao secao = listaSecoes.get(posicao);// Pega o item de acordo com a
		Secao secao = (Secao) getItem(posicao);	
		//holder._id.setText(secao.get_id());// posi��o.
		holder.str_sigla_secao.setText(secao.getStr_sigla_secao());
		holder.str_nome_secao.setText(secao.getStr_nome_secao());
		holder.str_sigla_organizacao.setText(secao.getStr_sigla_organizacao());
		holder.str_nome_organizacao.setText(secao.getStr_nome_organizacao());
		holder.telefone1.setText(secao.getTelefone1());
		holder.telefone2.setText(secao.getTelefone2());
		
		
		
		if (checkFavoriteItem(secao)) {
            holder.btnFavourite.setImageResource(R.drawable.ic_favorite);
            holder.btnFavourite.setTag("active");
        } else {
            holder.btnFavourite.setImageResource(R.drawable.ic_favorite_outline);
            holder.btnFavourite.setTag("deactive");
        }
		
		
		
		holder.btnFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String tag = holder.btnFavourite.getTag().toString();
                if (tag.equalsIgnoreCase("deactive")) {
                    sharedPreference.addFavorite(context, listaSecoes.get(posicao));
                    //Toast.makeText(context,context.getResources().getString(R.string.add_favr),Toast.LENGTH_SHORT).show();
                    
                    holder.btnFavourite.setTag("active");
                    holder.btnFavourite.setImageResource(R.drawable.ic_favorite);
                } else {
                    sharedPreference.removeFavorite(context, listaSecoes.get(posicao));
                   //Toast.makeText(context,context.getResources().getString(R.string.remove_favr),Toast.LENGTH_SHORT).show();
                    holder.btnFavourite.setTag("deactive");
                    holder.btnFavourite.setImageResource(R.drawable.ic_favorite_outline);
                }
            }
        });
		
		return convertView;
	}
	
	//Verifica se uma determinada secao existe em SharedPreferences
	 public boolean checkFavoriteItem(Secao checkProduct) {
         boolean check = false;//se for true todas imagens ficam selecionadas
         List<Secao> favorites = sharedPreference.loadFavorites(context);
         if (favorites != null) {
             for (Secao secao : favorites) {
                 if (secao.equals(checkProduct)) {
                     check = true;
                     break;
                 }
             }
         }
         return check;
     }
	

	static class ViewHolder {
		
		TextView str_sigla_secao;
		TextView str_nome_secao;
		TextView str_sigla_organizacao;
		TextView str_nome_organizacao;
		TextView telefone1;
		TextView telefone2;
		ImageView btnFavourite;

		public ViewHolder() {

		}
		
		
		 
		
	}
	
	public String formatPhone(String paramString)
	  {
	    String str1 = "";
	    paramString.replace("(", "");
	    paramString.replace(")", "");
	    paramString.replace("-", "");
	    paramString.replace("_", "");
	    paramString.replace(".", "");
	    paramString.replace(" ", "");
	    String str2 = paramString.trim();
	    if (str2.length() == 10) {
	      str1 = str1 + "(" + str2.substring(0, 2) + ") ";
	    }
	    for (String str3 = new StringBuilder(String.valueOf("")).append(str2.substring(2, 6)).append("-").toString() + str2.substring(6);; str3 = "" + str2) {
	      return str1 + str3;
	    }
	  }

	@Override
	public Filter getFilter() {
		Filter filter = new Filter() {

			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {

				listaSecoes = (List<Secao>) results.values;

				notifyDataSetChanged();

			}

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {

				FilterResults results = new FilterResults();
				
				List<Secao> FilteredArrList = new ArrayList<Secao>();

				if (mOriginalList == null) {
					mOriginalList = new ArrayList<Secao>(listaSecoes);
				}

				if (constraint == null || constraint.length() == 0) {
					// c�digo se por acaso o filtro for vazio
					// caso queira voltar a lista original use esse c�digo
					// results.count = mOriginalList.size();
					// results.values = mOriginalList;
                    // caso queria deix�-la vazia:
					listaSecoes.clear();
					results.count = 0;
					results.values = new ArrayList<Secao>();

				} else {
					constraint = constraint.toString().toLowerCase();
					for (int i = 0; i < mOriginalList.size(); i++) {
						Secao v = mOriginalList.get(i);
						String sigla_secao = v.getStr_sigla_secao();
						String nome_secao = v.getStr_nome_secao();
						String sigla_organizacao = v.getStr_sigla_organizacao();
						String nome_organizacao = v.getStr_nome_organizacao();
						//String telefone1 = v.getTelefone1(); Da erro na busca nullpointexception
						//String telefone2 = v.getTelefone2();
						
						//Funcionando 
						/*if (sigla_secao.toUpperCase().startsWith(
								constraint.toString().toUpperCase())) {
							FilteredArrList.add(v);
						}else

						if (nome_secao.toUpperCase().startsWith(
								constraint.toString().toUpperCase())) {
							FilteredArrList.add(v);
						}else

						if (sigla_organizacao.toUpperCase().startsWith(
								constraint.toString().toUpperCase())) {
							FilteredArrList.add(v);
						}else

						if (nome_organizacao.toUpperCase().startsWith(
								constraint.toString().toUpperCase())) {
							FilteredArrList.add(v);
						}*/
						
						if (sigla_organizacao.toLowerCase().startsWith(
								constraint.toString())) {
							FilteredArrList.add(v);
						}else
						
						if (sigla_secao.toLowerCase().startsWith(
								constraint.toString())) {
							FilteredArrList.add(v);
						}else

						if (nome_secao.toLowerCase().startsWith(
								constraint.toString())) {
							FilteredArrList.add(v);
						}else

						

						if (nome_organizacao.toLowerCase().startsWith(
								constraint.toString())) {
							FilteredArrList.add(v);
						}
							

					}

					results.count = FilteredArrList.size();
					results.values = FilteredArrList;
				}
				return results;
			}
		};
		return filter;
	}
}